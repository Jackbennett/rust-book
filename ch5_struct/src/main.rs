#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
    unit: String
}

// named tuple
struct WidthHeight(u32, u32);

fn create_rect(size: WidthHeight) -> Rectangle {
    Rectangle {
        width: size.0,
        height: size.1,
        unit: String::from("px")
    }
}

// multiple impl block are allowed, although not required here.
impl Rectangle {
    // Associated frunction, no reference to self
    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
            unit: String::from("px")
        }
    }
    // Method, self is first param
    fn area(&self) -> u32{
        &self.width * &self.height
    }

    fn can_fit(&self, rect: &Rectangle) -> bool{
        (self.width > rect.width) && (self.height > rect.height)
    }
}

use std::fmt;
impl fmt::Display for Rectangle {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{},{}{}", self.width, self.height, self.unit)
    }
}

fn main() {
    let rect1 = Rectangle {
        width: 50,
        height: 75,
        unit: String::from("px")
    };

    let rect2 = create_rect(WidthHeight(40,60));
    let rect3 = create_rect(WidthHeight(90,70));
    let square = Rectangle::square(30);

    println!("Starting rect1 {:#?} has an area of {}{}", rect1, rect1.area(),rect1.unit);

    println!("Rect {} can fit within {}: {}", rect2, rect1, rect1.can_fit(&rect2));
    println!("Rect {} can fit within {}: {}", rect3, rect1, rect1.can_fit(&rect3));

    println!("This is a shortcut to a Square {}", square);
}
