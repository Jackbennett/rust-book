Working through the rust book

# Notes / Cheatsheet

- Packages: A Cargo feature that lets you build, test, and share crates
- Crates: A tree of modules that produces a library or executable
- Modules and use: Let you control the organization, scope, and privacy of paths
- Paths: A way of naming an item, such as a struct, function, or module

- Package: Can contain multiple binary crates and/or one library crate
- `src/main.rs::main()` is the default main entry point for cargo
- `src/lib.rs tells cargo` the package has a library of the same name
- Relative paths enable moving the
- Absolute paths preferred (`crate::`) as its more likely to move code definitions around
- `super::` references the crates parent create as a starting path.

# Code

### Guessing_game

Chapter 2 task

### [Chapter 3](ch3_try_it)

Language constructs for control flow for, while, loop

# Rust Ideas

1. Search all the book pages for references to "Chapter" and link up the chapters that reference each other in a nice chart.
1. Search all commit messages to total up my `item: <amount>` tally while completing the the book
