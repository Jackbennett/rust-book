fn main() {
    println!("Chapter 8 Try_it! Collections section on Vectors, HashMaps and strings");
    println!("First up, maths - ");

    let num_range = vec![0, 1, 2, 3, 4, 5];
    println!("range of numbers({}): {:?}", num_range.len(), num_range);

    let math = get_math(&num_range);
    println!("Mean: {}", math.mean);
    println!("Median: {}", math.median);
    match math.mode {
        Some(val) => println!("Mode: {}", val),
        None => println!("No mode, occurences equal"),
    }
}

struct Math {
    mean: f32,           // average
    median: f32,         // middle of the sorted range
    mode: Option<isize>, // most common
}

use core::panic;
use std::collections::HashMap;
fn get_math(numbers: &[isize]) -> Math {
    let mut sum = 0_isize;
    for n in numbers {
        sum += n;
    }
    let mean = sum as f32 / numbers.len() as f32;

    let mut count_vals: HashMap<&isize, u32> = HashMap::new();
    for n in numbers {
        let count = count_vals.entry(n).or_insert(0);
        *count += 1;
    }

    let mode;
    if count_vals
        .values()
        .cloned()
        .collect::<Vec<u32>>()
        .windows(2)
        .all(|w| w[0] == w[1])
    {
        mode = None;
    } else {
        mode = count_vals
            .iter()
            .max_by(|a, b| a.1.cmp(b.1))
            .map(|(k, _v)| **k);
    }

    let mut sorted_input: Vec<isize> = numbers.iter().cloned().collect();
    sorted_input.sort();
    println!("median: {:?}", sorted_input);

    let midpoint = sorted_input.len() / 2;
    let median = match sorted_input.len() % 2 {
        r if r == 1 => (sorted_input[midpoint - 1] + sorted_input[midpoint]) as f32 / 2.0,
        r if r == 0 => sorted_input[midpoint] as f32,
        _ => panic!("Remainder value should not have been reached."),
    };

    Math { mean, median, mode }
}

#[test]
fn same_numbers() {
    let r = get_math(&vec![1, 1, 1, 1]);
    assert_eq!(None, r.mode);
    assert_eq!(1.0, r.mean);
    assert_eq!(1.0, r.median);
}

#[test]
fn even_array_mean() {
    let r = get_math(&vec![0, 1, 2, 3, 4, 5]);
    assert_eq!(2.5, r.mean);
}
#[test]
fn even_array_median() {
    let r = get_math(&vec![0, 1, 2, 3, 4, 5]);
    assert_eq!(2.5, r.median);
}

#[test]
fn odd_array_mean() {
    let r = get_math(&vec![0, 1, 2, 3, 4, 5, 6]);
    assert_eq!(3.0, r.mean);
}

#[test]
fn odd_array_median() {
    let r = get_math(&vec![0, 1, 2, 3, 4, 5, 6]);
    assert_eq!(3.0, r.median);
}
#[test]
fn unordered_array() {
    let r = get_math(&vec![9, 1, 2, 3, 4, 5, 6]);
    assert_eq!(4.2857142857143, r.mean);
    assert_eq!(4.0, r.median);
}

#[test]
fn same_in_array() {
    let r = get_math(&vec![1, 1, 2, 2, 3, 3]);
    assert_eq!(None, r.mode);
}
#[test]
fn mode_array() {
    let r = get_math(&vec![1, 2, 2, 2, 3, 3]);
    assert_eq!(2, r.mode.unwrap());
}
