// use of ! makes this a crate-level hint for #[warn(dead_code)]
#![allow(dead_code)]

mod front_of_house;

mod back_of_house {
    // user can select bread, but it's up to the chef about the fruit
    pub struct Breakfast{
        pub toast: String,
        seasonal_fruit: String
    }
    // A pub enum makes all variants public
    pub enum Appetizer {
        Soup,
        Salad
    }

    impl Breakfast {
        pub fn summer(toast: &str) -> Breakfast {
            Breakfast {
                toast: String::from(toast),
                seasonal_fruit: String::from("peaches")
            }
        }
    }

    fn cook_order(){
        super::ring_serving_bell()
    }
    fn fix_incorrcet_order(){
        cook_order();
        super::front_of_house::serving::serve_order();
    }
}

// Serving bell can be used by back or front staff.g
fn ring_serving_bell(){}

use front_of_house::hosting::add_to_waitlist as waitlist;

// pub here re-exports add_to_waitlist for consumers of my-package
// who may not care about the front/back of house disctinction.
pub use crate::front_of_house::hosting;

// fn is sibling of the module, so front_of_house doesn't require pub
pub fn eat_at_restaurant(){

    let _order1 = back_of_house::Appetizer::Soup;
    let _order2 = back_of_house::Appetizer::Salad;

    // order a summer breakfast
    let mut meal = back_of_house::Breakfast::summer("Rye");
    // change bread order
    meal.toast = String::from("Wheat");
    println!("I'd like {} toast please", meal.toast);

    // Cannot do this directly as it's private, see compiler error
    // meal.seasonal_fruit = String::from("Blackberry");


    // Absolute path reference
    crate::front_of_house::hosting::add_to_waitlist();
    // Relative
    front_of_house::hosting::add_to_waitlist();
    // re-assigned as (bad idea)
    waitlist();

    // idiomatic way to use
    hosting::add_to_waitlist();
}