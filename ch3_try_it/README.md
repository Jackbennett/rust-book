You learned about variables, scalar and compound data types, functions, comments, if expressions, and loops! If you want to practice with the concepts discussed in this chapter, try building programs to do the following:

- [x] Convert temperatures between Fahrenheit and Celsius.
- [ ] Generate the nth Fibonacci number.
- [ ] Print the lyrics to the Christmas carol "The Twelve Days of Christmas" taking advantage of the repetition in the song.
