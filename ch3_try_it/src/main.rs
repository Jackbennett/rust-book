use std::io;

mod program;
use program::*;

fn main_menu() {
    println!("Select program");
    for (i, option) in program::LIST.iter().enumerate() {
        // 1-based list for humans
        println!("\t{}. {}", i + 1, option)
    }
    println!("\t{}. {}", 0, "Exit")
}

fn main() {
    main_menu();
    loop {
        let mut choice = String::new();
        io::stdin()
            .read_line(&mut choice)
            .expect("Failed to process choice");

        let menu_item: usize = match choice.trim().parse() {
            Ok(val) => val,
            Err(_) => {
                println!("Choice ({}) was not a valid number", choice.trim());
                continue;
            }
        };

        match menu_item {
            0 => {
                println!("Closing!");
                break;
            }
            1 => {
                temparature();
                main_menu();
            }
            2 => {
                calc_fib();
                main_menu();
            }
            3 => {
                sing();
                main_menu();
            }
            _ => println!("Selection was not a menu option"),
        }
    }
}

fn temparature() {
    println!("Enter a temparature to convert");
    let mut choice = String::new();

    io::stdin()
        .read_line(&mut choice)
        .expect("Failed to process choice");

    let temp: isize = choice.trim().parse().expect("Choice was not a number");

    let degrees_c = convert_temp::F_to_C(temp);
    let degrees_f = convert_temp::C_to_F(temp);

    println!(
        "You wanted to convert {}. That's {}°C or {}°F",
        temp, degrees_c, degrees_f
    );
}

fn calc_fib(){
    println!("Enter a how many numbers of the sequence you want:");
    let mut choice = String::new();

    io::stdin()
        .read_line(&mut choice)
        .expect("Failed to process choice");

    let length: usize = choice.trim().parse().expect("Choice was not a number");
    let series = fibonacci::series(length);
    println!("First {} Fibonacci numbers: {:?}", length, series);
}

fn sing(){
    println!("Now the lines to the 12 days of Christmas");
    xmas_carrol::days_of_christmas();
}