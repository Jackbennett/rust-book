// 1+1+2+3+5+8+13+...

pub fn series(length: usize) -> Vec<usize> {
    let mut result: Vec<usize> = Vec::with_capacity(length);
    result.push(0);
    result.push(1);
    result.push(1);

    for count in 3..length {
        result.push(result[count - 1] + result[count - 2]);
    }

    result
}
