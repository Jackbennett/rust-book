macro_rules! song_line {
    () => ("On the {0} day of Christmas my true love sent to me")
}
const DAYS_OF_CHRISTMAS: [(&str, &str); 12] = [
    ("First", "A partitge in a pear tree"),
    ("Second","Two Turtle Doves"),
    ("Third","Three French Hens"),
    ("Fourth","Four Calling Birds"),
    ("Fift","Five Golden Rings"),
    ("Sixth","Six Geese a-laying"),
    ("Seventh","Seven Swans a-swimming"),
    ("Eighth","Eight Maids a-milking"),
    ("Nineth","Nine Ladies dancing"),
    ("Ten","Ten Lords a-leaping"),
    ("Eleventh","Eleven Pipers piping"),
    ("Twelth"," Twelve Drummers drumming"),
];

pub fn days_of_christmas(){
    for (count, stanza) in DAYS_OF_CHRISTMAS.iter().enumerate() {
        let (day, lines) = stanza;
        println!(song_line!(), day);
        println!("{}", lines);
        for (i, lines) in DAYS_OF_CHRISTMAS[0..count].iter().rev().enumerate() {
            if i == count-1 {
                println!("And {}", lines.1);
            } else {
                println!("{}", lines.1);
            }
        }
        println!(); // This line is intentionally left blank
    }
}