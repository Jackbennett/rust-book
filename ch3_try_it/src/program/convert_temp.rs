// (0°C × 9/5) + 32 = 32°F
#[allow(non_snake_case)]
pub fn C_to_F(C: isize) -> isize {
    (C * 9 / 5) + 32
}

// (32°F − 32) × 5/9 = 0°C
#[allow(non_snake_case)]
pub fn F_to_C(F: isize) -> isize {
    (F - 32) * 5 / 9
}
