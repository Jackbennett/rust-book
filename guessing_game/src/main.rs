use rand::{thread_rng, Rng};
use std::{cmp::Ordering, io};

fn main() {
    let number = thread_rng().gen_range(1..1000 + 1);
    println!("Welcome to the guessing game!");
    println!("To start, Enter a number between 1 and 1000");

    loop {
        let mut guess = String::new();
        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let guess: u32 = match guess.trim().parse() {
            Ok(input) => input,
            Err(_) => continue
        };

        match guess.cmp(&number) {
            Ordering::Less => print!("That's too low."),
            Ordering::Equal => {
                println!("Bingo!");
                break;
            }
            Ordering::Greater => print!("That's too high!"),
        }

        println!(" Guess again...");
    }
}
